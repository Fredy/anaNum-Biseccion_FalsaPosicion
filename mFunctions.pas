unit mFunctions;

interface

uses math;

function func1(x : real) : real;
function func2(x : real) : real;
function func3(x : real) : real;

implementation

function func1(x : real) : real;
begin
   func1 := x * x - ln(x) * exp(x);
end;

function func2(x : real) : real;
begin
   func2 := x * x - exp(x);
end;

function func3(x : real) : real;
begin
   //func3 := 1/(x - 1);
   func3 := ln(x)//sin(1/x); //1/(x-3)x(x-8)
end;

end.
