unit mMethods;

interface

uses math;

type
   nonLinearFunc = function(x : real) : real;
   methodFunc = function(a, b: real; func : nonLinearFunc): real;


function bisectionFunc(a, b: real; func : nonLinearFunc): real;
function falsePositionFunc(a, b: real; func : nonLinearFunc): real;

implementation

function bisectionFunc(a, b: real; func : nonLinearFunc): real;
begin
   bisectionFunc := 0.5 * (a + b);
end;

function falsePositionFunc(a, b: real; func : nonLinearFunc): real;
var
   fa, fb : real;
begin
   fa := func(a);
   fb := func(b);
   falsePositionFunc := a - fa * (b - a) / (fb - fa);
end;

end.
