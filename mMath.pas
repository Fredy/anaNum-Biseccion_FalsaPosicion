unit mMath;

interface

function fact(n : integer): int64;

implementation

function fact(n : integer): int64;
var
   i   : integer;
   res : int64;
begin
   res := 1;
   for i := 1 to n do
      res := res * i;
   fact := res;
end;

end.
