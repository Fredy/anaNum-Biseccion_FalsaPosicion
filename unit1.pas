unit Unit1;

{$mode objfpc}{$H+}

interface

uses
   Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
   Grids, mError, mFunctions, mMethods, math;

type

   { TForm1 }
   TForm1 = class(TForm)
      BtnGenTable: TButton;
      CbFunction: TComboBox;
      CbMethod: TComboBox;
      EditLowLimit: TEdit;
      EditHighLimit: TEdit;
      EditMinError: TEdit;
      Label1: TLabel;
      Label2: TLabel;
      Label3: TLabel;
      Label4: TLabel;
      Label5: TLabel;
      DataTable: TStringGrid;
      procedure BtnGenTableClick(Sender: TObject);
      procedure CbFunctionChange(Sender: TObject);
      procedure CbMethodChange(Sender: TObject);
      procedure EditLowLimitChange(Sender: TObject);
      procedure FormCreate(Sender: TObject);
      procedure CleanStringGrid();
   private
      funcArr : array[0..2] of nonLinearFunc;

      function isNanorInf(const d: Single) :boolean ;overload;
      function isNanorInf(const d: double) :boolean ;overload;
      function isNanorInf(const d: extended) :boolean ;overload;
   public
      { public declarations }
   end;

var
   Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.CbMethodChange(Sender: TObject);
begin

end;

procedure TForm1.EditLowLimitChange(Sender: TObject);
begin

end;

procedure TForm1.FormCreate(Sender: TObject);
begin
   funcArr[0] := @func1;
   funcArr[1] := @func2;
   funcArr[2] := @func3;

   CbFunction.Clear;
   CbFunction.Items.Add('x² - ln(x)eˣ');
   CbFunction.Items.Add('x² - eˣ');
   CbFunction.Items.Add('Profe');
   CbFunction.ItemIndex := 0;

   CbMethod.Items.Add('Bisección');
   CbMethod.Items.Add('Falsa Posición');
   CbMethod.ItemIndex := 0;
end;

procedure TForm1.CbFunctionChange(Sender: TObject);
begin

end;

procedure TForm1.BtnGenTableClick(Sender: TObject);
var
   minError, currentErr : real;
   a, b, fa, xn, fx, prevXn : real;
   usedMethod : methodFunc;
   funcIndex : Integer;
   tableRow : Integer;
   sgnA, sgnB : Integer;
   test1, test2 : real;
const
   realFormat = '0.#####';
begin
   if EditLowLimit.GetTextLen = 0 then
   begin
      ShowMessage('No ha insertado un límite inferior.');
      exit;
   end;

   if EditHighLimit.GetTextLen = 0 then
   begin
      ShowMessage('No ha insertado un límite superior.');
      exit;
   end;

   if EditMinError.GetTextLen = 0 then
   begin
      ShowMessage('No ha insertado el error mínimo.');
      exit;
   end;

   //TODO: check if the function is continous in the invertal!!!

   a := StrToFloat(EditLowLimit.Text);
   b := StrToFloat(EditHighLimit.Text);
   funcIndex := CbFunction.ItemIndex;

   test1 := funcArr[funcIndex](a);
   test2 := funcArr[funcIndex](b);

   if (IsNan(test1)) or (IsInfinite(test1)) then
   begin
      ShowMessage('ERROR: ' + FloatToStr(test1) + sLineBreak + 'La función no es continua en el primer límite.' );
      exit;
   end;

   if (IsNan(test2)) or (IsInfinite(test2)) then
   begin
      ShowMessage('ERROR: ' + FloatToStr(test2) + sLineBreak + 'La función no es continua en el segundo límite.');
      exit;
   end;

   sgnA := Sign(funcArr[funcIndex](a));
   sgnB := Sign(funcArr[funcIndex](b));

   if (sgnA = sgnB) then
   begin
      ShowMessage('El teorema de Bolzano no se cumple para los límites ingresados.');
      exit;
   end;

   if (sgnA = 0) then
   begin
      ShowMessage('La respuesta es: ' + FloatToStr(a) );
      exit;
   end;

   if (sgnB = 0) then
   begin
      ShowMessage('La respuesta es: ' + FloatToStr(b) );
      exit;
   end;

   CleanStringGrid();

   minError := StrToFloat(EditMinError.Text);

   case CbMethod.ItemIndex of
      0 : usedMethod := @bisectionFunc;
      1 : usedMethod := @falsePositionFunc;
   end;

   tableRow := 1;
   currentErr := minError;
   while currentErr >= minError do
   begin
      prevXn := xn;
      xn := usedMethod(a, b, funcArr[funcIndex]);
      fa := funcArr[funcIndex](a);
      fx := funcArr[funcIndex](xn);

      ///
      if (isNanorInf(fa) or isNanorInf(fx))then
      begin
         ShowMessage('No hay solución');
         exit;
      end;
      ///
      DataTable.InsertRowWithValues(tableRow,[]);
      DataTable.Cells[0,tableRow] := IntToStr(tableRow - 1);
      DataTable.Cells[1,tableRow] := FormatFloat(realFormat, a);
      DataTable.Cells[2,tableRow] := FormatFloat(realFormat, b);

      if Sign(fa) = Sign(fx) then
      begin
         a := xn;
         DataTable.Cells[3,tableRow] := '+';
      end
      else
      begin
         b := xn;
         DataTable.Cells[3,tableRow] := '-';
      end;

      DataTable.Cells[4,tableRow] := FormatFloat(realFormat, xn);

      if tableRow > 1 then

      begin
         currentErr := absError(xn, prevXn);
         DataTable.Cells[5,tableRow] := FormatFloat(realFormat, currentErr);
         DataTable.Cells[6,tableRow] := FormatFloat(realFormat, relError(xn,prevXn));
         DataTable.Cells[7,tableRow] := FormatFloat(realFormat, perError(xn, prevXn)) + '%';
      end;

      tableRow := tableRow + 1;
   end;
   // Más iteraciones para comprobar respuesta
   while (currentErr >= 0.00000000000000000000000001) do
   begin
      prevXn := xn;
      xn := usedMethod(a, b, funcArr[funcIndex]);
      fa := funcArr[funcIndex](a);
      fx := funcArr[funcIndex](xn);

      ///
      if (isNanorInf(fa) or isNanorInf(fx))then
      begin
         ShowMessage('No hay solución');
         exit;
      end;
      ///
      if Sign(fa) = Sign(fx) then
         a := xn
      else
         b := xn;
      currentErr := absError(xn, prevXn);
   end;
end;

procedure TForm1.CleanStringGrid;
var
   I: Integer;
begin
   for I := 0 to DataTable.ColCount - 1 do
      DataTable.Cols[I].Clear;
   DataTable.RowCount := 1;
end;

function TForm1.isNanorInf(const d: Single): boolean;
begin
   Result := (isNan(d) or isInfinite(d));
end;

function TForm1.isNanorInf(const d: double): boolean;
begin
   Result := (isNan(d) or isInfinite(d));
end;

function TForm1.isNanorInf(const d: extended): boolean;
begin
    Result := (isNan(d) or isInfinite(d));
end;

end.
